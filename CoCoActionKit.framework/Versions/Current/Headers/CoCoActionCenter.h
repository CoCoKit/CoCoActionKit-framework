//
//  CoCoActionCenter.h
//  CoCoActionKit
//
//  Created by 陈明 on 2018/5/1.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoCoActionContext.h"


@interface CoCoActionCenter : NSObject
+ (id)createControllerWithContext:(CoCoActionContext *)context delegate:(id)delegate parames:(id)obj, ... NS_REQUIRES_NIL_TERMINATION;
@end

//
//  CoCoActionContext.h
//  CoCoActionKit
//
//  Created by 陈明 on 2018/5/1.
//  Copyright © 2018年 陈明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoActionContext : NSObject
- (NSString *)viewControllerName;
- (NSString *)initializationName;
- (NSString *)delegateName;
@property (nonatomic, strong) NSMutableDictionary *keyValues;
@end
